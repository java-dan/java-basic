package hw09.humans;

import hw09.Human;
import hw09.DayOfWeek;

import java.util.Map;

import static libs.Console.print;

public final class Woman extends Human {
    public Woman(String name, String surname, String birthDay, Integer iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDay, iq, schedule);
    }
    public Woman(String name, String surname, String birthDay) {
        super(name, surname, birthDay);
    }
    public Woman() {
        super();
    }
    public void makeup () {
        print("Пора накраситься");
    }
    @Override
    public void greetPet () {
        print(String.format("Рад тебя видеть, %s", this.getFamily().getPets()));
    }
}
