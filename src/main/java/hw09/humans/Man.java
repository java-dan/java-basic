package hw09.humans;

import hw09.Human;
import hw09.DayOfWeek;

import java.util.Map;

import static libs.Console.print;

public final class Man extends Human {
    public Man(String name, String surname, String birthDay, Integer iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDay, iq, schedule);
    }
    public Man(String name, String surname, String birthDay) {
        super(name, surname, birthDay);
    }
    public Man() {
        super();
    }
    public void repairCar () {
        print("Я ремонтирую автомобиль.");
    }
    @Override
    public void greetPet () {
        print(String.format("Рад тебя видеть, %s", this.getFamily().getPets()));
    }
}
