package hw09.animals;

import hw09.FoulMaker;
import hw09.Pet;
import hw09.Species;

import java.util.Set;

import static libs.Console.print;

public class DomesticCat extends Pet implements FoulMaker {
    final Species species = Species.DOMESTIC_CAT;
    public DomesticCat (String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }
    public DomesticCat (String nickname) {
        super(nickname);
    }
    public DomesticCat () {
        super();
    }

    @Override
    public void foul() {
        print("Покопаюсь в вазонах)");
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void respond() {
        print(String.format("Meow, хозяин. Я - %s. Я соскучился!", this.getNickname()));
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
            this.getSpecies(),
            this.getNickname(),
            this.getAge(),
            this.getTrickLevel(),
            this.getHabits()
        );
    }
}
