package hw09.animals;

import hw09.Pet;
import hw09.Species;

import java.util.Set;

import static libs.Console.print;

public class RoboCat extends Pet {
    final Species species = Species.ROBO_CAT;
    public RoboCat (String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }
    public RoboCat (String nickname) {
        super(nickname);
    }
    public RoboCat () {
        super();
    }
    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void respond() {
        print(String.format("Привет, хозяин. Я - %s. Я помыл пол!", this.getNickname()));
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
            this.getSpecies(),
            this.getNickname(),
            this.getAge(),
            this.getTrickLevel(),
            this.getHabits()
        );
    }
}
