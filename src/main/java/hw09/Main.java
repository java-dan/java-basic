package hw09;

import hw09.humans.Man;

import static libs.Console.print;

public class Main {
    public static void main(String[] args) {
        Man f1 = new Man("Bob", "Jey", "10/02/1978");
        print(f1.describeAge());
        print(f1.toString());
    }
}
