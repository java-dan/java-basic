package hw09;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static libs.Console.print;

public class Human {
    private String name;
    private String surname;
    public final long birthDay;
    private Integer iq;
    public final Map<DayOfWeek, String> schedule;
    private Family family;
    public Human (String name, String surname, String birthDay, Integer iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDay = getBirthdayMilis(birthDay);
        this.iq = iq;
        this.schedule = schedule;
    }
    public Human (String name, String surname, String birthDay, Integer iq) { this(name, surname, birthDay, iq, new HashMap<>()); }
    public Human (String name, String surname, String birthDay) {
        this(name, surname, birthDay, 0, new HashMap<>());
    }
    public Human () {this("", "", "01/01/0001");}
    public String  getName () { return  name; }
    public void setName (String name) { this.name = name; }
    public String getSurname () { return  surname; }
    public void setSurname (String surname) { this.surname = surname; }
    public long getBirthDay () { return  birthDay; }
    public Integer getIq () { return  iq; }
    private long getBirthdayMilis (String birthDay) {
        try {
            SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
            Date d = sd.parse(birthDay);
            return d.getTime();
        } catch (ParseException ex) {
            System.out.println(ex);
        }
        return 0;
    }
    public void setIq (Integer iq) {
        if (iq >= 0 && iq <= 100) this.iq = iq;
    }
    public Map<DayOfWeek, String> getSchedule () { return  schedule; }
    public Family getFamily () { return  this.family; }
    public void setFamily (Family family) {
        this.family = family;
    }
    public String  describeAge () {
        LocalDate now = LocalDate.now(ZoneId.systemDefault());
        LocalDate date = LocalDate.ofInstant(Instant.ofEpochMilli(birthDay), ZoneId.systemDefault());
        Period p = Period.between(date, now);
        return String.format("%s years, %s months, %s days", p.getYears(), p.getMonths(), p.getDays());
    }
    public void greetPet() {
        print(String.format("Привет, %s", this.getFamily().getPets()));
    }
    public void describePet () {
        String message = "";
        Set<Pet> pets = this.getFamily().getPets();
        if(pets == null) return;
        pets.forEach(pet -> {
            if(pet.getTrickLevel() <= 50) {
                print(String.format("%s почти не хитрый", pet.getNickname()));
            } else {
                print(String.format("%s очень хитрый", pet.getNickname()));
            }
        });
    }
    public void feedPet () {
        print(String.format("Хм... покормлю ка я %s", this.getFamily().getPets()));
    }
    public String birthDateToString () {
        LocalDate date = LocalDate.ofInstant(Instant.ofEpochMilli(birthDay), ZoneId.systemDefault());
        return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }
    @Override
    public String toString () {
        return String.format(
                "Human{name='%s', surname='%s', date of birth=%s, iq=%d, schedule=%s}",
                this.getName(), this.getSurname(), this.birthDateToString() , this.getIq(), this.getSchedule());
    }
    @Override
    public int hashCode () {
        return (int) this.getBirthDay() + 27 + this.getName().hashCode();
    }
    @Override
    public boolean equals (Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;

        if (!(obj instanceof Human h)) return false;

        return this.getName().equals(h.getName()) &&
//                this.getBirthDay().equals(h.getBirthDay()) &&
                this.getSurname().equals(h.getSurname());
    }
    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
