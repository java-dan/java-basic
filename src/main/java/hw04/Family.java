package hw04;

import java.util.Arrays;

import static libs.Console.print;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;

        this.mother.setFamily(this);
        this.father.setFamily(this);
    }
    Family (Human mother, Human father) {
        this(mother, father, new Human[]{}, new Pet());
    }

    public Human getMother () { return mother; }
    public void setMother (Human mother) { this.mother = mother; }
    public Human getFather () { return father; }
    public void setFather (Human father) { this.father = father; }
    public Human[] getChildren () { return children; }
    public void setChildren (Human[] children) { this.children = children; }
    public Pet getPet () { return pet; }
    public void setPet (Pet pet) { this.pet = pet; }
    public void addChild (Human child) {
        this.children = Arrays.copyOf(this.getChildren(), this.getChildren().length + 1);
        this.children[this.children.length - 1] = child;
        child.setFamily(this);
    }
    public Boolean deleteChild (int i) {
        if (i > this.children.length - 1 || i < 0) return false;
        Human child = this.children[i];
        child.setFamily(null);

        Human[] childrenNew = new Human[this.children.length - 1];
        for (int ind = 0, k = 0; ind < this.children.length; ind++) {
            if (ind != i){
                childrenNew[k] = this.children[ind];
                k++;
            }
        }
        this.children = childrenNew;

        return true;
    }
    public int countFamily () { return 2 + this.children.length; }

    @Override
    public String toString() {
        return String.format("Mother {%s}:\nFather {%s};\nChildren {%s};\nPet {%s}",
            this.mother, this.father, Arrays.toString(this.children), this.pet);
    }
    @Override
    public int hashCode () {
        return this.getMother().hashCode() * 2 + this.getFather().hashCode() * 2 + 19;
    }
    @Override
    public boolean equals (Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;

        if (!(obj instanceof Family f)) return false;

        return this.getFather().equals(f.getFather()) &&
                this.getMother().equals(f.getMother()) &&
                Arrays.equals(this.getChildren(), f.getChildren());
    }
}
