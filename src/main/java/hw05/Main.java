package hw05;

import java.util.Arrays;

import static hw05.DayOfWeek.*;
import static hw05.Species.*;
import static libs.Console.print;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) { // working with 1000000
            new Human();
        }
//        System.gc();

        Pet p1 = new Pet(CAT, "Lisa", 7, 90, new String[]{"eating, playing"});
        Human m1 = new Human("Lili", "Jey", 1990);
        Human f1 = new Human("Bob", "Jey", 1990);
        String [][] s1 = new String[][] {{SUNDAY.name(), "cinema"}};
        m1.setSchedule(s1);
        Family fam1 = new Family(m1, f1);
        fam1.setPet(p1);
        print(fam1.toString());

        Pet p3 = new Pet(CAT, "Lisa");
        Human m3 = new Human("Lili", "Jey", 1990);
        Human f3 = new Human("Bob", "Jey", 1990);
        Family fam3 = new Family(m3, f3);
        fam3.setPet(p3);
        print(fam3.toString());
        print(String.format("Equals %s",fam3.equals(fam1)));

        Pet p2 = new Pet();
        Human m2 = new Human("Kate", "Walter", 1976, 99, new String[][]{{WEDNESDAY.name(), "water flowers"}, {FRIDAY.name(), "go to the bar"}});
        Human f2 = new Human("Jim", "Honey", 1966, 89, new String[][]{{FRIDAY.name(), "go to the bar"}});
        Human c21 = new Human("Charlie", "Honey", 2003);
        Human c22 = new Human("Sam", "Honey", 2005);
        Family fam2 = new Family(m2, f2, new Human[]{c21, c22}, p2);
        print(fam2.toString());
        print(String.format("Members of family %d",fam2.countFamily()));

        p2.setSpecies(DOG);
        p2.setNickname("Bax");
        p2.setAge(2);
        p2.setTrickLevel(73);
        p2.setHabits(new String[] {"playing, jumping, eating"});


        Human c23 = new Human("Diana", "Walter", 2000);
        c23.setFamily(fam2);
        fam2.addChild(c23);
        print(fam2.toString());
        print(String.format("Members of family %d",fam2.countFamily()));

        fam2.deleteChild(1);
        print(fam2.toString());
        print(String.format("Members of family %d",fam2.countFamily()));

        Human c24 = new Human();
        fam2.addChild(c24);
        print(fam2.toString());
        print(String.format("Members of family %d",fam2.countFamily()));

        c24.setName("Mark");
        c24.setSurname("Walter");
        c24.setYear(2009);
        c24.setIq(73);
        c24.setSchedule(new String[][]{
                {MONDAY.name(), "do homework"},
                {WEDNESDAY.name(), "football"}
        });
        c24.setFamily(fam2);

        print(c24.getName());
        print(c24.getSurname());
        print(c24.getYear().toString());
        print(String.format("%d", c24.getIq()));
        print(Arrays.deepToString(c24.getSchedule()));
        print(c24.getFamily().toString());

        print(c24.toString());
        c24.greetPet();
        c24.describePet();
        c24.feedPet();
        print(String.valueOf(c24.hashCode()));
        print(String.valueOf(c24.equals(c23)));

        p2.respond();
        p2.eat();
        p2.foul();
        print(p2.toString());
        print(String.valueOf(p2.hashCode()));
        print(String.valueOf(p2.equals(p1)));

        print(c24.getFamily().toString());
    }
}
