package hw05;

public enum Species {
    DOG,
    CAT,
    HAMSTER,
    FISH,
    PARROT
}
