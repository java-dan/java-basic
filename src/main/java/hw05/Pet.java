package hw05;

import java.util.Arrays;
import static libs.Console.print;

public class Pet {
    private Species species;
    private String nickname;
    private Integer age;
    private Integer trickLevel;
    private String[] habits;

    public Pet (Species species, String nickname, Integer age, Integer trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Pet (Species species, String nickname) {
        this(species, nickname, 0, 0, new String[] {});
    }
    public Pet () {}

    public Species getSpecies () { return  species; }
    public void setSpecies (Species species) { this.species = species; }
    public String getNickname () { return  nickname; }
    public void setNickname (String nickname) { this.nickname = nickname; }
    public Integer getAge () { return  age; }
    public void setAge (Integer age) {
        if (age >= 0 && age <= 130) this.age = age;
    }
    public Integer getTrickLevel () { return  trickLevel; }
    public void setTrickLevel (Integer trickLevel) {
        if (trickLevel >= 0 && trickLevel <= 100) this.trickLevel = trickLevel;
    }
    public String[] getHabits () { return  habits; }
    public void setHabits (String[] habits) { this.habits = habits; }


    public void eat () { print("Я кушаю!"); }
    public void respond() { print(String.format("Привет, хозяин. Я - %s. Я соскучился!", this.getNickname())); }
    public void foul() { print("Нужно хорошо замести следы..."); }

    @Override
    public String toString () {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
                this.getSpecies(),
                this.getNickname(),
                this.getAge(),
                this.getTrickLevel(),
                Arrays.toString(this.getHabits())
        );
    }
    @Override
    public int hashCode () {
        return this.getAge() * 3 + this.getNickname().hashCode() + this.getSpecies().hashCode() + 17;
    }
    @Override
    public boolean equals (Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;

        if (!(obj instanceof Pet p)) return false;

        return this.getNickname().equals(p.getNickname()) &&
                this.getAge().equals(p.getAge()) &&
                this.getSpecies().equals(p.getSpecies());
    }
    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
