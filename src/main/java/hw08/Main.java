package hw08;

import hw08.animals.Dog;
import hw08.animals.RoboCat;
import hw08.humans.Woman;
import hw08.humans.Man;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static libs.Console.print;

public class Main {
    public static void main(String[] args) {
        Set<String> habitsP1 = new HashSet<>();
        habitsP1.add("eating");
        habitsP1.add("playing");
        Set<String> habitsP2 = new HashSet<>();
        habitsP2.add("wash a floor");
        habitsP2.add("playing music");
        Pet p1 = new Dog("Lisa", 7, 90, habitsP1);
        Pet p2 = new RoboCat("Monica", 3, 45, habitsP2);
        print(p1.toString());

        Map<DayOfWeek, String> schedulerM1= new HashMap<>();
        Woman m1 = new Woman("Lili", "Jey", 1990, 97, schedulerM1);
        schedulerM1.put(DayOfWeek.FRIDAY, "go to the cinema");
        schedulerM1.put(DayOfWeek.SUNDAY, "spa day");
        schedulerM1.put(DayOfWeek.WEDNESDAY, "clean the house");
        print(m1.toString());

        Man f1 = new Man("Bob", "Jey", 1990);
        Family fam1 = new Family(m1, f1);
        Human c1 = new Human("Charlie", "Honey", 2003);
        Human c2 = new Human("Sam", "Honey", 2005);
        Human c3 = new Human("Lisa", "Honey", 2007);
        fam1.addChild(c1);
        fam1.addChild(c2);
        fam1.addChild(c3);
        print(fam1.getChildren().toString());
        fam1.deleteChild(c2);
        print(fam1.getChildren().toString());
        fam1.deleteChild(1);
        print(fam1.getChildren().toString());
        fam1.addNewPet(p1);
        fam1.addNewPet(p2);
        print(fam1.toString());
    }
}
