package hw08;

import hw08.humans.Woman;
import hw08.humans.Man;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Family {
    private Woman mother;
    private Man father;
    public final List<Human> children;
    public final Set<Pet> pets;
    public Family(Woman mother, Man father, List<Human> children, Set<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pets = pets;

        this.mother.setFamily(this);
        this.father.setFamily(this);
    }
    public Family(Woman mother, Man father) { this(mother, father, new ArrayList<>(), new HashSet<>()); }
    public Woman getMother () { return mother; }
    public void setMother (Woman mother) { this.mother = mother; }
    public Man getFather () { return father; }
    public void setFather (Man father) { this.father = father; }
    public List<Human> getChildren () { return children; }
    public Set<Pet> getPets () { return pets; }
    public void addChild (Human child) {
        this.children.add(child);
        child.setFamily(this);
    }
    public boolean deleteChild (int i) {
        if (i > this.children.size() || i < 0) return false;
        Human child = this.children.remove(i);
        child.setFamily(null);
        return true;
    }
    public Boolean deleteChild (Human child) {
        try {
            if (this.children.size() == 0 ) return false;
            List<Human> childrenNew = new ArrayList<>(this.getChildren());
            child.setFamily(null);
            this.children.remove(child);
            if(childrenNew.size() == this.children.size()) return  false;
            return true;
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
    }
    public int countFamily () { return 2 + this.children.size(); }
    public void addNewPet(Pet pet) {
        pets.add(pet);
    }
    public void deletePet(Pet pet) {
        pets.remove(pet);
    }

    @Override
    public String toString() {
        return String.format("Mother {%s}:\nFather {%s};\nChildren {%s};\nPet {%s}",
                this.mother, this.father, this.children, this.pets);
    }
    @Override
    public int hashCode () {
        return this.getMother().hashCode() * 2 + this.getFather().hashCode() * 2 + 19;
    }
    @Override
    public boolean equals (Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;

        if (!(obj instanceof Family f)) return false;

        return this.getFather().equals(f.getFather()) &&
                this.getMother().equals(f.getMother()) &&
                this.getChildren().equals(f.getChildren());
    }
    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
