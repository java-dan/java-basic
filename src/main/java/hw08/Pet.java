package hw08;

import java.util.HashSet;
import java.util.Set;

import static libs.Console.print;

public abstract class Pet {
    public final String nickname;
    private Integer age;
    private Integer trickLevel;
    public final Set<String> habits;
    public Pet (String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        Species species = Species.UNKNOWN;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Pet (String nickname) {
        this(nickname, 0, 0, new HashSet<>() );
    }
    public Pet () {
        this("");
    }
    public abstract Species getSpecies ();
    public String getNickname () { return  nickname; }
    public Integer getAge () { return  age; }
    public void setAge (Integer age) {
        if (age >= 0 && age <= 130) this.age = age;
    }
    public Integer getTrickLevel () { return  trickLevel; }
    public void setTrickLevel (Integer trickLevel) {
        if (trickLevel >= 0 && trickLevel <= 100) this.trickLevel = trickLevel;
    }
    public Set<String> getHabits () { return  habits; }

    public void eat () { print("Я кушаю!"); }
    public abstract void respond();
    public abstract String toString();

    @Override
    public int hashCode () {
        return this.getAge() * 3 + this.getNickname().hashCode() + 17;
    }
    @Override
    public boolean equals (Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;

        if (!(obj instanceof Pet p)) return false;

        return this.getNickname().equals(p.getNickname()) &&
                this.getAge().equals(p.getAge());
    }
    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
