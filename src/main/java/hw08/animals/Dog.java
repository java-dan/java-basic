package hw08.animals;

import hw08.FoulMaker;
import hw08.Pet;
import hw08.Species;

import java.util.Set;

import static libs.Console.print;

public class Dog extends Pet implements FoulMaker {
    final Species species = Species.DOG;
    public Dog (String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }
    public Dog (String nickname) {
        super(nickname);
    }
    public Dog () {
        super();
    }
    @Override
    public void foul() {
        print("Пойду пожую кроссовки)");
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void respond() {
        print(String.format("Woof, хозяин. Я - %s. Я соскучился!", this.getNickname()));
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
            this.getSpecies(),
            this.getNickname(),
            this.getAge(),
            this.getTrickLevel(),
            this.getHabits()
        );
    }
}
