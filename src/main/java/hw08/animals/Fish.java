package hw08.animals;

import hw08.Species;
import hw08.Pet;

import java.util.Set;

import static libs.Console.print;

public class Fish extends Pet {
    final Species species = Species.FISH;
    public Fish (String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }
    public Fish (String nickname) {
        super(nickname);
    }
    public Fish () {
        super();
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void respond() {
        print(String.format("Привет! Я %s %s.", this.getSpecies(), this.getNickname()));
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
            this.getSpecies(),
            this.getNickname(),
            this.getAge(),
            this.getTrickLevel(),
            this.getHabits()
        );
    }
}
