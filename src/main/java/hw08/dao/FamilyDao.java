package hw08.dao;

import hw08.Family;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies() throws Exception;
    Family getFamilyByIndex(int id) throws Exception;
    boolean deleteFamily(int id);
    boolean deleteFamily(Family family);
    void saveFamily(Family family);
}
