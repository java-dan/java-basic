package hw08.dao;

public interface Identifable {
    int getId();
}
