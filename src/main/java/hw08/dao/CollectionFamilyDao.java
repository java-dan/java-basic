package hw08.dao;

import hw08.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao{
    private final List<Family> familyList = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() throws Exception {
        return List.copyOf(familyList);
    }

    @Override
    public Family getFamilyByIndex(int id) throws Exception {
        return null;
    }

    @Override
    public boolean deleteFamily(int id) {
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return false;
    }

    @Override
    public void saveFamily(Family family) {

    }
}
