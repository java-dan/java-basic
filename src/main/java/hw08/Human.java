package hw08;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static libs.Console.print;

public class Human {
    private String name;
    private String surname;
    private Integer year;
    private Integer iq;
    public final Map<DayOfWeek, String> schedule;
    private Family family;
    public Human (String name, String surname, Integer year, Integer iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }
    public Human (String name, String surname, Integer year) {
        this(name, surname, year, 0, new HashMap<>());
    }
    public Human () {this("", "", 0);}
    public String  getName () { return  name; }
    public void setName (String name) { this.name = name; }
    public String getSurname () { return  surname; }
    public void setSurname (String surname) { this.surname = surname; }
    public Integer getYear () { return  year; }
    public void setYear (Integer year) { this.year = year; }
    public Integer getIq () { return  iq; }
    public void setIq (Integer iq) {
        if (iq >= 0 && iq <= 100) this.iq = iq;
    }
    public Map<DayOfWeek, String> getSchedule () { return  schedule; }
    public Family getFamily () { return  this.family; }
    public void setFamily (Family family) {
        this.family = family;
    }
    public void greetPet() {
        print(String.format("Привет, %s", this.getFamily().getPets()));
    }
    public void describePet () {
        String message = "";
        Set<Pet> pets = this.getFamily().getPets();
        if(pets == null) return;
        pets.forEach(pet -> {
            if(pet.getTrickLevel() <= 50) {
                print(String.format("%s почти не хитрый", pet.getNickname()));
            } else {
                print(String.format("%s очень хитрый", pet.getNickname()));
            }
        });
    }
    public void feedPet () {
        print(String.format("Хм... покормлю ка я %s", this.getFamily().getPets()));
    }
    @Override
    public String toString () {
        return String.format(
                "Human{name='%s', surname='%s', year=%d, iq=%d, schedule=%s}",
                this.getName(), this.getSurname(), this.getYear(), this.getIq(), this.getSchedule());
    }
    @Override
    public int hashCode () {
        return this.getYear() + 27 + this.getName().hashCode();
    }
    @Override
    public boolean equals (Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;

        if (!(obj instanceof hw06.Human h)) return false;

        return this.getName().equals(h.getName()) &&
                this.getYear().equals(h.getYear()) &&
                this.getSurname().equals(h.getSurname());
    }
    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
