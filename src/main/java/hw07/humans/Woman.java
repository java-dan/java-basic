package hw07.humans;

import hw07.DayOfWeek;
import hw07.Human;

import java.util.Map;

import static libs.Console.print;

public final class Woman extends Human {
    public Woman(String name, String surname, Integer year, Integer iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }
    public Woman(String name, String surname, Integer year) {
        super(name, surname, year);
    }
    public Woman() {
        super();
    }
    public void makeup () {
        print("Пора накраситься");
    }
    @Override
    public void greetPet () {
        print(String.format("Рад тебя видеть, %s", this.getFamily().getPets()));
    }
}
