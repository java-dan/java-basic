package hw06;

import java.util.Arrays;
import static libs.Console.print;

public class Human {
    private String name;
    private String surname;
    private Integer year;
    private Integer iq;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, Integer year, Integer iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }
    public Human(String name, String surname, Integer year) {
        this(name, surname, year, 0, new String[][] {{}});
    }
    public Human() {}

    public String  getName () { return  name; }
    public void setName (String name) { this.name = name; }
    public String getSurname () { return  surname; }
    public void setSurname (String surname) { this.surname = surname; }
    public Integer getYear () { return  year; }
    public void setYear (Integer year) { this.year = year; }
    public Integer getIq () { return  iq; }
    public void setIq (Integer iq) {
        if (iq >= 0 && iq <= 100) this.iq = iq;
    }
    public String[][] getSchedule () { return  schedule; }
    public void setSchedule (String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily () { return  this.family; }
    public void setFamily (Family family) {
        this.family = family;
    }
    public void greetPet() {
        print(String.format("Привет, %s", this.getFamily().getPet().getNickname()));
    }
    public void describePet () {
        String message = "";
        Pet pet = this.getFamily().getPet();
        if(pet == null) return;
        if (pet.getTrickLevel() <= 50) {
            message = "почти не хитрый";
        } else {
            message = "очень хитрый";
        }
        print(String.format("У меня есть %s, ему %d лет, он %s", this.getFamily().getPet().getSpecies(), this.getFamily().getPet().getAge(), message));
    }
    public void feedPet () {
        print(String.format("Хм... покормлю ка я %s", this.getFamily().getPet().getNickname()));
    }

    @Override
    public String toString () {
        return String.format(
                "Human{name='%s', surname='%s', year=%d, iq=%d, schedule=%s}",
                this.getName(), this.getSurname(), this.getYear(), this.getIq(), Arrays.deepToString(this.getSchedule()));
    }
    @Override
    public int hashCode () {
        return this.getYear() + 27 + this.getName().hashCode();
    }
    @Override
    public boolean equals (Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;

        if (!(obj instanceof Human h)) return false;

        return this.getName().equals(h.getName()) &&
                this.getYear().equals(h.getYear()) &&
                this.getSurname().equals(h.getSurname());
    }
    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
