package hw06;

public enum Species {
    DOG,
    DOMESTIC_CAT,
    ROBO_CAT,
    FISH,
    UNKNOWN
}
