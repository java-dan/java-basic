package hw06.animals;

import hw06.FoulMaker;
import hw06.Pet;
import hw06.Species;

import java.util.Arrays;

import static libs.Console.print;

public class DomesticCat extends Pet implements FoulMaker {
    final Species species = Species.DOMESTIC_CAT;

    public DomesticCat (String nickname, Integer age, Integer trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }
    public DomesticCat (String nickname) {
        super(nickname);
    }
    public DomesticCat () {
        super();
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void foul() {
        print("Покопаюсь в вазонах)");
    }

    @Override
    public void respond() {
        print(String.format("Meow, хозяин. Я - %s. Я соскучился!", this.getNickname()));
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
            this.getSpecies(),
            this.getNickname(),
            this.getAge(),
            this.getTrickLevel(),
            Arrays.toString(this.getHabits())
        );
    }
}
