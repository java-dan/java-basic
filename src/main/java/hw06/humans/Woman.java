package hw06.humans;

import hw06.Human;

import static libs.Console.print;

public final class Woman extends Human {
    public Woman(String name, String surname, Integer year, Integer iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }
    public Woman(String name, String surname, Integer year) {
        super(name, surname, year);
    }
    public Woman() {
        super();
    }

    public void makeup () {
        print("Пора накраситься");
    }
    @Override
    public void greetPet () {
        print(String.format("Рад тебя видеть, %s", this.getFamily().getPet().getNickname()));
    }
}
