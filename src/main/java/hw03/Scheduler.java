package hw03;

public class Scheduler {
    public static String[][] schedule = {
            {"sunday", "do home work"},
            {"monday", "go to courses; watch a film"},
            {"tuesday", "water flowers"},
            {"wednesday", "go to courses"},
            {"thursday", "clean windows"},
            {"friday", "go to the cinema"},
            {"saturday", "go to the ocean"},
    };

    private static Integer getIndex (String line) {
        return switch (line.toLowerCase()) {
            case "sunday" -> 0;
            case "monday" -> 1;
            case "tuesday" -> 2;
            case "wednesday" -> 3;
            case "thursday" -> 4;
            case "friday" -> 5;
            case "saturday" -> 6;
            default -> -1;
        };
    }

    public String getTask (String line) {
        int index = getIndex(line.toLowerCase());

        return schedule[index][1];
    }
}
