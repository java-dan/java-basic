package hw03;

import java.util.Scanner;

import static libs.Console.print;

public class TaskPlanner {
    public static boolean isLetter(String name) {
        return name.matches("[a-zA-Z]+");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String userInput = "";
        String task = "";
        Scheduler scheduler = new Scheduler();

        while (!userInput.equals("exit")) {
            print("Please, input the day of the week:");
            try {
                userInput = sc.nextLine().trim();
                if (userInput.equals("exit")) break;
                if(!isLetter(userInput)) {
                    print("Sorry, I don't understand you, please try again.");
                } else {
                    task = scheduler.getTask(userInput);
                    print(task);
                }
            } catch (RuntimeException e) {
                System.out.println("Not a weekday.");
            }
        }
    }
}
