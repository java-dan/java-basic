package hw02;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Utils {
    public static int random(int min, int max) {
        int range = max - min + 1;
        double random = Math.random() * range;
        return (int) (random + min);
    }

    public static int readIntScanner (Scanner sc) {
        int num = 0;
        boolean valid = false;
        while(! valid ) {
            System.out.print("Enter number between 1 and 5: ");
            try {
                num = sc.nextInt();
                if(num >= 1 && num <= 5){
                    valid = true;
                } else {
                    System.out.println("Out of range.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Not a valid number.");
                sc.next();
            }
        }
        return num;
    }
}
