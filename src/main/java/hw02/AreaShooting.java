package hw02;

import java.util.Scanner;

import static hw02.Utils.random;
import static hw02.Utils.readIntScanner;
import static libs.Console.print;

public class AreaShooting {
    public static String representCell(int value) {
        if (value == 1) return " * ";
        if (value == 2) return " X ";
        return " - ";
    }
    private static void printArea(int[][] area) {
        for (int y = 0; y < area.length; y++) {
            for (int x = 0; x < area[y].length; x++) {
                if(y == 0) {
                    System.out.printf("  %s  |", x);
                } else if(x == 0) {
                    System.out.printf("  %s  |", y);
                } else {
                    System.out.printf(" %s |", representCell(area[y][x]));
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int[][] area = new int[6][6];
        int x = random(1,5);
        int y = random(1,5);
        int userX;
        int userY;

        print("All set. Get ready to rumble!");
        System.out.printf("%d:%d", x, y);
        do {
            print("Enter 'x' coordinates: ");
            userX = readIntScanner(sc);

            print("Enter 'y' coordinates: ");
            userY = readIntScanner(sc);

            area[userY][userX] = 1;
            if(userX == x && userY == y) {
                area[y][x] = 2;
            }
            printArea(area);


        } while (userX != x || userY != y);
        print("You have won!");
    }
}
