package hw01;

import java.util.Arrays;
import java.util.Scanner;

import static libs.Console.print;

public class RandomNumbers {

    public static int random(int min, int max) {
        int range = max - min + 1;
        double random = Math.random() * range;
        int result = (int) (random + min);
        return result;
    }

    public static int[] reverse(int[] data) {
        int[] outcome = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            outcome[i] = data[data.length - i - 1];
        }
        return outcome;
    }

    public static void main(String[] args) {
        print("Let the game begin!");
        print("Enter your name: ");

        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();

        int r = random(0, 100);

        System.out.printf("%d, %s\n", r, name);
        int[] intsArr = new int[0];
        int value = 0;

        do {
            print("Enter the number from 0 to 100: ");
            while(!sc.hasNextInt()) {
                System.out.println("That's not a number!\nEnter the number from 0 to 100: ");
                sc.next();
            }
            value = sc.nextInt();

            intsArr = Arrays.copyOf(intsArr, intsArr.length+1);
            intsArr[intsArr.length-1] = value;

            if(value < r) {
                print("Your number is too small. Please, try again.");
            } else if (value > r) {
                print("Your number is too big. Please, try again.");
            }
        } while(value != r);

        System.out.printf("Congratulations, %s!\n", name);

        Arrays.sort(intsArr);
        int[] reversed = reverse(intsArr);
        System.out.printf("Your numbers: %s!\n", Arrays.toString(reversed));

    }
}
