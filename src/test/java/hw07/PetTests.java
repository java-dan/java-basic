package hw07;

import hw07.animals.Dog;
import hw07.animals.DomesticCat;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class PetTests {
    Set<String> hP3 = new HashSet<>();
    Pet p = new Dog("Dollar");
    Pet p2 = new Dog("Dollar");
    Pet p3 = new DomesticCat("Maxi", 2, 73, hP3);
    @Test
    public void checkPetToString() {
        hP3.add("play with a ball");
        String expect = "DOMESTIC_CAT{nickname='Maxi', age=2, trickLevel=73, habits=[play with a ball]}";
        assertEquals(expect, p3.toString());
    }
    @Test
    public void petEquals() {
        assertEquals(p, p2);
        assertNotEquals(p, p3);
    }
}
