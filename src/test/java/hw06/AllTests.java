package hw06;

import hw06.animals.Dog;
import hw06.animals.DomesticCat;
import hw06.humans.Man;
import hw06.humans.Woman;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AllTests {
    Woman m = new Woman("Lili", "Jey", 1990);
    Man f = new Man("Bob", "Jey", 1990);
    Human c1 = new Human("Max", "Jey", 2010);
    Human c2 = new Human("Max", "Jey", 2013);
    Human c3 = new Human("Max", "Jey", 2015);
    Pet p = new Dog("Bax");

    @Test
    public  void petToString () {
        String expected = "DOG{nickname='Bax', age=0, trickLevel=0, habits=[]}";
        String result = p.toString();

        assertEquals(expected, result);
    }

    @Test
    public void addChild () {
        Family fam1 = new Family(m, f);
        fam1.addChild(c1);

        assertEquals(1, fam1.getChildren().length);
        assertNotEquals(2, fam1.getChildren().length);

        assertEquals(fam1.getChildren()[0], c1);
        assertNotEquals(fam1.getChildren()[0], c2);
    }

    @Test
    public void deleteChild () {
        Family fam2 = new Family(m, f);
        fam2.addChild(c1);
        fam2.addChild(c2);

        assertEquals(2, fam2.getChildren().length);

        fam2.deleteChild(1);
        assertEquals(1, fam2.getChildren().length);

        fam2.deleteChild(3);
        assertEquals(1, fam2.getChildren().length);
    }

    @Test
    public void deleteChildByObj () {
        Family fam2 = new Family(m, f);
        fam2.addChild(c1);
        fam2.addChild(c2);

        assertEquals(2, fam2.getChildren().length);

        Boolean result1 = fam2.deleteChild(c3);
        assertEquals(2, fam2.getChildren().length);
        assertFalse(result1);

        Boolean result2 = fam2.deleteChild(c2);
        assertEquals(1, fam2.getChildren().length);
        assertTrue(result2);
    }

    @Test
    public void countFamily () {
        Family fam3 = new Family(m, f);
        fam3.addChild(c1);
        fam3.addChild(c2);

        int result1 = fam3.countFamily();
        assertEquals(4, result1);
        assertNotEquals(5, result1);
    }

    @Test
    public void checkString () {
        Pet p = new DomesticCat("Chichi");
        Family fam4 = new Family(m, f);
        fam4.addChild(c3);
        fam4.setPet(p);

        String expHuman = "Human{name='Lili', surname='Jey', year=1990, iq=0, schedule=[[]]}";
        String expFamily = "Mother {Human{name='Lili', surname='Jey', year=1990, iq=0, schedule=[[]]}}:\n" +
                "Father {Human{name='Bob', surname='Jey', year=1990, iq=0, schedule=[[]]}};\n" +
                "Children {[Human{name='Max', surname='Jey', year=2015, iq=0, schedule=[[]]}]};\n" +
                "Pet {DOMESTIC_CAT{nickname='Chichi', age=0, trickLevel=0, habits=[]}}";
        String expPet = "DOMESTIC_CAT{nickname='Chichi', age=0, trickLevel=0, habits=[]}";

        String resultHuman = m.toString();
        String resultFamily = fam4.toString();
        String resultPet = p.toString();

        assertEquals(expHuman, resultHuman);
        assertEquals(expFamily, resultFamily);
        assertEquals(expPet, resultPet);

        assertNotEquals(expHuman, resultFamily);
        assertNotEquals(expFamily, resultPet);
        assertNotEquals(expPet, resultHuman);
    }
}
